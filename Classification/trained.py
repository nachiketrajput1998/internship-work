import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torchvision
import torchvision.transforms as transforms
import torchvision.datasets as datasets
import os
import random
import numpy as np
import torchvision.models as models
from tensorboardX import SummaryWriter
from torch.utils.data.sampler import SubsetRandomSampler

writer = SummaryWriter()

train_transforms = transforms.Compose([
                           transforms.RandomHorizontalFlip(),
                           transforms.ToTensor(),
                       ])

test_transforms = transforms.Compose([
                           transforms.ToTensor(),
                       ])


train_data = datasets.ImageFolder('Train', train_transforms)
#valid_data = datasets.ImageFolder('validation', test_transforms)
test_data = datasets.ImageFolder('Test', test_transforms)


manual_seed = random.randint(1, 10000)
random.seed(manual_seed)
torch.manual_seed(manual_seed)

validation_split = .2
dataset_size = len(test_data)
indices = list(range(dataset_size))
split = int(np.floor(validation_split * dataset_size))
np.random.seed(manual_seed)
np.random.shuffle(indices)
test_split, val_split = indices[split:], indices[:split]


# Creating PT data samplers and loaders:
test_sampler = SubsetRandomSampler(test_split)
valid_sampler = SubsetRandomSampler(val_split)



test_iterator = torch.utils.data.DataLoader(dataset=test_data,batch_size=1,sampler=test_sampler)
valid_iterator = torch.utils.data.DataLoader(dataset=test_data, batch_size=1,sampler=valid_sampler)
train_iterator = torch.utils.data.DataLoader(dataset=train_data,batch_size=4,shuffle=True, num_workers=4)


device = torch.device('cuda')


#model whose weights are being updated 
model = models.resnet18(pretrained=True).to(device)


for param in model.parameters():
    param.requires_grad = False


for param in model.fc.parameters():
    param.requires_grad = True


model.fc = nn.Linear(in_features=512, out_features=2).to(device)



optimizer = optim.Adam(model.parameters(),lr = 0.01)

criterion = nn.CrossEntropyLoss()

def calculate_accuracy(fx, y):
    preds = fx.max(1, keepdim=True)[1]
    correct = preds.eq(y.view_as(preds)).sum()
    acc = correct.float()/preds.shape[0]
    return acc


def train(model, device, iterator, optimizer, criterion):
    
    epoch_loss = 0
    epoch_acc = 0    
    model.train()    
    for (x, y) in iterator:        
        x = x.to(device)
        y = y.to(device)        
        optimizer.zero_grad()                
        fx = model(x)        
        loss = criterion(fx, y)        
        acc = calculate_accuracy(fx, y)        
        loss.backward()        
        optimizer.step()        
        epoch_loss += loss.item()
        epoch_acc += acc.item()        
    return epoch_loss / len(iterator), epoch_acc / len(iterator)


def evaluate(model, device, iterator, criterion):    
    epoch_loss = 0
    epoch_acc = 0    
    model.eval()    
    with torch.no_grad():
        for (x, y) in iterator:
            x = x.to(device)
            y = y.to(device)
            fx = model(x)
            loss = criterion(fx, y)
            acc = calculate_accuracy(fx, y)
            epoch_loss += loss.item()
            epoch_acc += acc.item()
        
    return epoch_loss / len(iterator), epoch_acc / len(iterator)


EPOCHS = 50

best_valid_loss = float('inf')

MODEL_SAVE_PATH = 'model.pt'

best_valid_loss = float('inf')


for epoch in range(EPOCHS):
    train_loss, train_acc = train(model, device, train_iterator, optimizer, criterion)
    val_loss, val_acc = evaluate(model, device, valid_iterator, criterion)

    if val_loss < best_valid_loss:
        best_valid_loss = val_loss
        torch.save(model.state_dict(), MODEL_SAVE_PATH)
    writer.add_scalar('fc/train(loss)', train_loss,epoch)
    writer.add_scalar('fc/val(loss)', val_loss,epoch)
    print("Epoch number {} : Train loss {} : Train Accu {} : val. loss {} : Val. Accu {} :".format(epoch+1,train_loss,train_acc*100,val_loss,val_acc*100))

writer.close()


model.load_state_dict(torch.load(MODEL_SAVE_PATH))


train_loss, train_acc = train(model, device, train_iterator, optimizer, criterion)

print("Train loss {} : Train Acc. {}".format(train_loss,train_acc*100))

test_loss, test_acc = evaluate(model, device, test_iterator, criterion)

print("Test loss {} : Test Acc. {}".format(test_loss,test_acc*100))

val_loss, val_acc = evaluate(model, device, test_iterator, criterion)

print("validation loss {} : validation Acc. {}".format(val_loss,val_acc*100))