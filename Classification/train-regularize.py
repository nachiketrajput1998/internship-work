import torch
from torch import nn
from PIL import Image
from network import Network
from classdataset import ClassDataset
from config import Config
import torchvision.datasets as datasets
import os
import random
import numpy as np
import torchvision.transforms as transforms
from torch.utils.data import DataLoader,Dataset
import numpy as np
from torch import optim
import pickle
from torch.autograd import Variable
from accuracy import Accuracy
import torchvision.models as models
from torch.utils.data.sampler import SubsetRandomSampler
from tensorboardX import SummaryWriter

writer = SummaryWriter()

train_transforms = transforms.Compose([
                           transforms.RandomHorizontalFlip(),
                           transforms.ToTensor(),
                       ])

test_transforms = transforms.Compose([
                           transforms.ToTensor(),
                       ])



train_data = datasets.ImageFolder('Test', train_transforms)
#valid_data = datasets.ImageFolder('validation', test_transforms)
#test_data = datasets.ImageFolder('Test', test_transforms)

manual_seed = random.randint(1, 10000)
random.seed(manual_seed)
torch.manual_seed(manual_seed)

validation_split = .4
dataset_size = len(train_data)
indices = list(range(dataset_size))
split = int(np.floor(validation_split * dataset_size))
np.random.seed(manual_seed)
np.random.shuffle(indices)
train_split, val_split = indices[split:], indices[:split]



train_sampler = SubsetRandomSampler(train_split)
valid_sampler = SubsetRandomSampler(val_split)



train_iterator = torch.utils.data.DataLoader(dataset=train_data,batch_size=1,sampler=train_sampler)
valid_iterator = torch.utils.data.DataLoader(dataset=train_data, batch_size=1,sampler=valid_sampler)
#train_iterator = torch.utils.data.DataLoader(dataset=train_data,batch_size=4,shuffle=True, num_workers=4)
device = torch.device('cuda')


def calculate_accuracy(fx, y):
    preds = fx.max(1, keepdim=True)[1]
    correct = preds.eq(y.view_as(preds)).sum()
    acc = correct.float()/preds.shape[0]
    return acc

def train_regularize(model, device, iterator, optimizer, criterion):
    
    epoch_loss = 0
    epoch_acc = 0    
    model.train()    
    for (x, y) in iterator:        
        x = x.to(device)
        y = y.to(device)        
        optimizer.zero_grad()                
        fx = model(x)        
        loss = criterion(fx, y)
        l1 = 0
        lambda_l1 = 0.001

        for w,ws in zip(model.parameters(),model2):
                if str(ws) == 'fc.weight':
                        l1 = l1 + (w-model2.state_dict()[ws]).norm(2)
                        #l1 = l1 + (w-model2.state_dict()[ws]).abs().sum()
        loss = loss + lambda_l1 * l1

        loss.backward()
        optimizer.step()
        acc = calculate_accuracy(fx, y)        
        epoch_loss += loss.item()
        epoch_acc += acc.item()        
    return epoch_loss / len(iterator), epoch_acc / len(iterator)

def evaluate(model, device, iterator, criterion):    
    epoch_loss = 0
    epoch_acc = 0    
    model.eval()    
    with torch.no_grad():
        for (x, y) in iterator:
            x = x.to(device)
            y = y.to(device)
            fx = model(x)
            loss = criterion(fx, y)
            acc = calculate_accuracy(fx, y)
            epoch_loss += loss.item()
            epoch_acc += acc.item()
        
    return epoch_loss / len(iterator), epoch_acc / len(iterator)


#model whose weights are not being updated
model2 = torch.load('model.pt')
#model2.eval()
#print(model2)

'''
for param in model2.parameters():
    param.requires_grad = False
model2.fc = nn.Linear(in_features=512, out_features=2).to(device)
'''

#model whose weights are being updated 
model = models.resnet18(pretrained=True).to(device)

for param in model.fc.parameters():
    param.requires_grad = False

for param in model.fc.parameters():
    param.requires_grad = True

model.fc = nn.Linear(in_features=512, out_features=2).to(device)



print("During regularization")


EPOCHS = 40
best_valid_loss = float('inf')
MODEL_SAVE_PATH = 'model_reg.pt'

best_valid_loss = float('inf')
optimizer = optim.Adam(model.fc.parameters(),lr = 0.01)

criterion = nn.CrossEntropyLoss()


for epoch in range(EPOCHS):
    train_loss, train_acc = train_regularize(model, device, train_iterator, optimizer, criterion)
    valid_loss, valid_acc = evaluate(model, device, valid_iterator, criterion)

    if valid_loss < best_valid_loss:
        best_valid_loss = valid_loss
        torch.save(model.state_dict(), MODEL_SAVE_PATH)

    writer.add_scalar('fc/train_reg(loss)', train_loss,epoch)
    writer.add_scalar('fc/val_reg(loss)', valid_loss,epoch)    
    print("Epoch number {} : Train loss {} : Train Accu {} : val. loss {} : Val. Accu {} :".format(epoch+1,train_loss,train_acc*100,valid_loss,valid_acc*100))

writer.close()


#    print("Epoch number {} : Train loss {} : Train Accu {} : val. loss {} : Val. Accu {} :".format(epoch+1,train_loss,train_acc*100))

model.load_state_dict(torch.load(MODEL_SAVE_PATH))

val_loss, val_acc = evaluate(model, device, valid_iterator, criterion)

print("Test loss {} : Test Acc. {}".format(val_loss,val_acc*100))

