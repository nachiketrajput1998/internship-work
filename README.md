**Description**:

***Siamese Network*** :

Contains siamese.py with one shot learning mechanism. 
This algorithm is used while working with smaller dataset.
Here dataset used : SIGNET dataset.

For more info, visit [https://towardsdatascience.com/one-shot-learning-with-siamese-networks-using-keras-17f34e75bb3d](https://towardsdatascience.com/one-shot-learning-with-siamese-networks-using-keras-17f34e75bb3d)


***Custom Neural Network***:

Used custom neural network implemented in network.py
Use it and improve the accuracy of model for different datasets.


***Classification***:

trained.py - Implemented with RESNET18 and uses Bach-Cancer dataset.
train-regularize - It improves the accuracy of RESNET18 model using **transfer learning**. 
[Train](https://drive.google.com/open?id=1lRA0X4lvECtNAVS0CHMTQsvNGMAs4TS_) and [Test](https://drive.google.com/open?id=1s9YrrOTZGanV6ClBntWpli9xAVnPQPzj) dataset are availble on google drive. (download it)

Generate_Benign_patches.ipynb - Generate the small patches (400*400) of images to improve the accurcy during training.
Note: Change the train and test data path while running.


------------------------------------------------------------------------------------------------------------------------------------------------------

**Libraries Used**

Pytorch

