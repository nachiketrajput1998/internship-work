import torch
import torchvision
from torch import nn
from PIL import Image
import random
import torchvision.datasets as dset
import torchvision.transforms as transforms
from torch.utils.data import DataLoader,Dataset
import numpy as np
from torch import optim
import pickle

class ClassDataset(Dataset):
    
    def __init__(self,imageFolderDataset,transform=None):
        self.imageFolderDataset = imageFolderDataset    
        self.transform = transform
        
    def __getitem__(self,index):
        img0_tuple = random.choice(self.imageFolderDataset.imgs)
        img0 = Image.open(img0_tuple[0])  
        
        if self.transform is not None:
            img0 = self.transform(img0)
            
        return img0, int(img0_tuple[1])
    
    def __len__(self):
        return len(self.imageFolderDataset.imgs)
