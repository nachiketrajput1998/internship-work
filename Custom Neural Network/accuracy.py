import torch
import torchvision
from torch import nn
from PIL import Image
import random
import torchvision.datasets as dset
import torchvision.transforms as transforms
from torch.utils.data import DataLoader,Dataset
import numpy as np
from torch import optim
from network import Network
from classdataset import ClassDataset
from config import Config
from torch.autograd import Variable

model = Network().cuda()
model.load_state_dict(torch.load('class.pth'))
model.eval()


target_dataset = dset.ImageFolder(root=Config.testing_dir)

target_transform = ClassDataset(imageFolderDataset=target_dataset,
                                        transform=transforms.Compose([transforms.ToTensor()
                                                                      ])
                            )

target_dataloader = DataLoader(target_transform,
                        shuffle=True,
                        num_workers=8,
                        batch_size=Config.train_batch_size)

criteria = torch.nn.CrossEntropyLoss()
class Accuracy():
    def test(model,epoch):
        model.eval()
        test_loss = 0
        correct = 0
        for data, target in target_dataloader:
            data, target = Variable(data), Variable(target)
            data = data.cuda()
            target = target.cuda()
            output = model(data)
            # sum up batch loss
            test_loss += criteria(output, target)
            # get the index of the max
            pred = output.data.max(1, keepdim=True)[1]
            correct += pred.eq(target.data.view_as(pred)).cpu().sum()
    
        test_loss /= len(target_dataloader.dataset)
        print('Epoch number {} :Test set: Average loss: {:.4f}, Accuracy: {}/{} ({:.0f}%)\n'.format(
            epoch,test_loss, correct, len(target_dataloader.dataset),
            100. * correct / len(target_dataloader.dataset)))


#Accuracy.test(model)