import torch
import torchvision
from torch import nn
from PIL import Image
import random
import torchvision.datasets as dset
import torchvision.transforms as transforms
from torch.utils.data import DataLoader,Dataset
import numpy as np
from torch import optim
import pickle
from network import Network
from classdataset import ClassDataset
from config import Config
from tensorboardX import SummaryWriter
from accuracy import Accuracy

writer = SummaryWriter()

folder_dataset = dset.ImageFolder(root=Config.training_dir)

source_transform = ClassDataset(imageFolderDataset=folder_dataset,
                                        transform=transforms.Compose([transforms.ToTensor()
                                                                      ])
                            )

source_dataloader = DataLoader(source_transform,
                        shuffle=True,
                        num_workers=8,
                        batch_size=Config.train_batch_size)



model = Network().cuda()

optimizer = optim.SGD(model.parameters(),lr = 0.005)

criterion = torch.nn.CrossEntropyLoss()



counter = []
loss_history = [] 
iteration_number= 0
for epoch in range(0,Config.train_number_epochs):
    for i, data in enumerate(source_dataloader,0):
        img0,label = data
        img0,label = img0.cuda(),label.cuda()
        optimizer.zero_grad()
        output = model(img0)
        
        loss_criteria = criterion(output,label)
        loss_criteria.backward()
              
        # print('backward pass done!')
        optimizer.step()

        if i %10 == 0 :
            writer.add_scalar('data/scalar1', loss_criteria*10,epoch)
            print("Epoch number {} : Current loss {}".format(epoch,loss_criteria.item()))
            Accuracy.test(model,epoch)	
            iteration_number +=10
            counter.append(iteration_number)
            loss_history.append(loss_criteria.item())

torch.save(model.state_dict(),'class.pth')

