class Config():
    training_dir="Train/"
    testing_dir="Test/"
    validation_dir="validation/"	
    train_batch_size = 100
    train_number_epochs = 50
