import torch
import torchvision
from torch import nn
from PIL import Image
import random
import torchvision.datasets as dset
import torchvision.transforms as transforms
from torch.utils.data import DataLoader,Dataset
import numpy as np
from torch import optim
import pickle

class Network(nn.Module):
    def __init__(self):
        super(Network, self).__init__()
        self.conv1 = nn.Conv2d(in_channels=3, out_channels=16, kernel_size=5,padding=1,stride=1)
        self.pool1 =  nn.MaxPool2d(kernel_size=2, stride=2, padding=1)
        self.relu1 = nn.ReLU(True)
        nn.BatchNorm2d(16)
        nn.Dropout(0.5)

        
        self.conv2 = nn.Conv2d(in_channels=16, out_channels=32, kernel_size=5,padding=1,stride=1)
        self.pool2 =  nn.MaxPool2d(kernel_size=2, stride=2, padding=1)
        self.relu2 = nn.ReLU(True)
        nn.BatchNorm2d(32)
        nn.Dropout(0.5)


        self.conv3 = nn.Conv2d(in_channels=32, out_channels=64, kernel_size=5,padding=1,stride=1)
        self.pool3 =  nn.MaxPool2d(kernel_size=2, stride=2, padding=1)
        self.relu3 = nn.ReLU(True)
        nn.BatchNorm2d(64)
        nn.Dropout(0.5)
        '''
        self.conv4 = nn.Conv2d(in_channels=64, out_channels=128, kernel_size=5,padding=1,stride=1)
        self.pool4 =  nn.MaxPool2d(kernel_size=2, stride=2, padding=1)
        self.relu4 = nn.ReLU(True)
        nn.BatchNorm2d(128)
        nn.Dropout()
        '''
        self.fc1 = nn.Linear(in_features=64*50*50, out_features=120)
        nn.Dropout(0.5)	
        self.fc2 = nn.Linear(in_features=120, out_features=2)	

                
    def forward(self, t):

        out = self.conv1(t)
        out = self.relu1(out)
        out = self.pool1(out)

        out = self.conv2(out)
        out = self.relu2(out)
        out = self.pool2(out)

        out = self.conv3(out)
        out = self.relu3(out)        
        out = self.pool3(out)
    '''
        out = self.conv4(out)
        out = self.relu4(out)        
        out = self.pool4(out)
	'''
        out = out.view(out.shape[0],-1)
        
        out = self.fc1(out)        
        out = self.fc2(out)

        return out





