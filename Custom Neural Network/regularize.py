import torch
from torch import nn
from PIL import Image
from network import Network
from classdataset import ClassDataset
from config import Config
import torchvision.datasets as dset
import torchvision.transforms as transforms
from torch.utils.data import DataLoader,Dataset
import numpy as np
from torch import optim
import pickle
from torch.autograd import Variable
from accuracy import Accuracy

#from tensorboardX import SummaryWriter

writer = SummaryWriter()

model = Network().cuda()
model.load_state_dict(torch.load('class.pth'))
model.eval()

model_2 = Network().cuda()
model_2.load_state_dict(torch.load('class.pth'))
model_2.eval()

target_dataset = dset.ImageFolder(root=Config.testing_dir)

target_transform = ClassDataset(imageFolderDataset=target_dataset,
                                        transform=transforms.Compose([transforms.ToTensor()
                                                                      ])
                            )

target_dataloader = DataLoader(target_transform,
                        shuffle=True,
                        num_workers=8,
                        batch_size=Config.train_batch_size)


for param in model.parameters():
    param.requires_grad = False

#model.fc1 = nn.Linear(128*25*25,120)
model.fc2 = nn.Linear(120, 2)
model = model.cuda()

criteria = torch.nn.CrossEntropyLoss()
optimizer = optim.SGD(model.fc2.parameters(),lr = 0.005)

#Accuracy.test(model)

counter = []
loss_history = [] 
iteration_number= 0
for epoch in range(0,Config.train_number_epochs):
    for i, data in enumerate(target_dataloader,0):
        img0,label = data
        img0,label = img0.cuda(),label.cuda()

        optimizer.zero_grad()
        output = model(img0)

        loss = criteria(output,label)
        l1 = 0
        lambda_l1 = 0.001

        for w,ws in zip(model.parameters(),model_2.parameters()):
                if str(w) == 'fc2.weight' and str(ws) == 'fc2.weight':		
                                l1 = l1 + (model.state_dict()[w]-model_2.state_dict()[ws]).abs().sum()
        loss = loss + lambda_l1 * l1
        loss.backward()
        optimizer.step()
        if i %10 == 0 :
                writer.add_scalar('data/scalar2', loss,epoch)
                print("Epoch number {} : Current loss {}".format(epoch,loss.item()))
                iteration_number +=10
                counter.append(iteration_number)
                loss_history.append(loss.item())

torch.save(model.state_dict(),'class_reg.pth')
#Accuracy.test(model)
